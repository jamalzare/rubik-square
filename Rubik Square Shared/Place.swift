//
//  Place.swift
//  Rubik Square
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//


import Foundation
import SpriteKit

class Place: SKShapeNode{
    
    var square: SKShapeNode!
    
    var index = -1{
        didSet{
        }
    }
    var row = -1{
        didSet{
            
        }
    }
    var column = -1
    
    var color: NSColor = .clear{
        didSet{
            square.fillColor = color
        }
    }
    
    var testNumber: Int = 1{
        didSet{
        }
    }
    
    func createSquare(){
        let size = CGSize(width: frame.width * 0.9, height: frame.height * 0.9)
        square = SKShapeNode(rectOf: size, cornerRadius: 20)
        square.strokeColor = .clear
        addChild(square)
        createSquare2()
    }
    
    func createSquare2(){
        let size = CGSize(width: frame.width * 0.1, height: frame.height * 0.1)
        let square2 = SKShapeNode(rectOf: size, cornerRadius: 20)
        square2.strokeColor = .clear
        square2.fillColor = NSColor.init(red: 1, green: 1, blue: 1, alpha: 0.9)
        addChild(square2)
    }
    
    
    override init() {
        super.init()
        name = "place"
        strokeColor = .clear
        fillColor = .darkGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

