//
//  GameScene.swift
//  Rubik Square Shared
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit

var label: SKLabelNode!

func showText(_ text: String){
    label.text = text
}

var sceneSize = CGSize.zero

class GameScene: SKScene {
    
    var builder: PlacesBuilder!
    var sections: Sections!
    var move: Move!
    var randomMove: RandomMove!
    
    override init(size: CGSize) {
        sceneSize = size
        let maxApspectRatio: CGFloat = 16/9
        let playAbleWidth = size.height / maxApspectRatio
        let margin = (size.width - playAbleWidth)/2
        gameArea = CGRect(x: margin, y: 0, width: playAbleWidth, height: size.height)
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMove(to view: SKView) {
        buildGame()
    }
    
    func buildGame(){
        sections = Sections(area: gameArea)
        addChild(sections.node)
        label = sections.moveLabel
        builder = PlacesBuilder(area: sections.main.frame, rowsCount: 4, colsCount: 4)
        addChild(builder.node)
        
        move = Move(places: builder.places)
        randomMove = RandomMove(move: move)
    }
    
    func selectPlace(point: CGPoint) {
        let frame = sections.main.frame
        if point.x > frame.maxX || point.x < frame.minX
            || point.y > frame.maxY || point.y < frame.minY{
            randomMove.start()
            return
        }
        let nodes = builder.node.nodes(at: point)
        for node in nodes{
            if let place = node as? Place{
                move.selectPlace(place: place, point: point)
            }
        }
    }
    
    func movePlaces(point: CGPoint){
        let frame = sections.main.frame
        if point.x > frame.maxX || point.x < frame.minX
            || point.y > frame.maxY || point.y < frame.minY{
            return
        }
        move.movePlaces(point: point)
    }
    
    func alignPlaces(point: CGPoint){
        move.alignPosition()
    }
    
}


#if os(OSX)
    extension GameScene {
        
        override func mouseDown(with event: NSEvent) {
            selectPlace(point: event.location(in: self))
        }
        
        override func mouseDragged(with event: NSEvent) {
            movePlaces(point: event.location(in: self))
        }
        
        override func mouseUp(with event: NSEvent) {
            alignPlaces(point: event.location(in: self))
        }
        
        
    }
#endif
