//
//  Sections.swift
//  Rubik Square
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class Sections {
    
    var area: CGRect!
    var node: SKShapeNode!
    var main: SKShapeNode!
    var moveLabel: SKLabelNode!
    
    var moveCount: Int = 0{
        didSet{
            moveLabel.text = "Move: \(moveCount)"
        }
    }
    
    init(area: CGRect) {
        self.area = area
        createGameArea()
        createMain()
        createLabel()
        createHeader()
        createFooter()
        createLeftSide()
        createRightSide()
    }
    
    func createGameArea(){
        let shape = SKShapeNode(rect: area)
        shape.fillColor = themColor
        node = shape
    }
    
    func createMain(){
        let side = area.width - 40
        let size = CGSize(width: side, height: side)
        main = SKShapeNode(rectOf: size)
        main.fillColor = themColor
        main.strokeColor = .clear
        main.lineWidth = 40
        node.addChild(main)
        main.position = CGPoint(x: area.midX, y: area.midY)
    }
    
    func createHeader(){
        let rect = CGRect(x: 0,
                          y: main.frame.maxY + 8,
                          width: sceneSize.width,
                          height:  area.height - main.frame.maxY)
        createSection(rect: rect)
    }
    
    func createFooter() {
        let rect = CGRect(x: 0, y: 0,
                          width: sceneSize.width,
                          height: main.frame.minY - 8)
        createSection(rect: rect)
    }
    
    func createLeftSide(){
        let rect = CGRect(x: 0, y: 0,
                          width: main.frame.minX,
                          height: area.height)
        createSection(rect: rect)
    }
    
    func createRightSide(){
        let rect = CGRect(x: main.frame.maxX, y: 0,
                          width: sceneSize.width - main.frame.maxX,
                          height: area.height)
        createSection(rect: rect)
    }
    
    func createSection(rect: CGRect){
        let shape = SKShapeNode(rect: rect)
        shape.fillColor = themColor
        node.addChild(shape)
        shape.zPosition = 1
        shape.strokeColor = .clear
    }
    
    func createLabel(){
        moveLabel = SKLabelNode(text: "Move")
        moveLabel.fontColor = .yellow
        moveLabel.fontSize = 80
        moveLabel.fontName = "Baloo"
        node.addChild(moveLabel)
        moveLabel.position = CGPoint(x: area.minX + 10, y: area.maxY - moveLabel.frame.height - 30)
        moveLabel.color = .blue
        moveLabel.horizontalAlignmentMode = .left
        moveCount = 0
    }
}
