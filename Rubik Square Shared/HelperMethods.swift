//
//  HelperMethods.swift
//  Rubik Square
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

var gameArea: CGRect = CGRect.zero
var themColor: NSColor = .gray

func randomCGFloat(_ lowerLimit: CGFloat, _ upperLimit: CGFloat)-> CGFloat{
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upperLimit - lowerLimit) + lowerLimit
}

func randomCGFloat(_ upperLimit: CGFloat)-> CGFloat{
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upperLimit)
}

func randomInt(_ upperLimit: Int)-> Int{
    return Int(randomCGFloat(CGFloat(upperLimit)))
}

func randomInt(_ lowerLimit: Int, _ upperLimit: Int)-> Int{
    return Int(randomCGFloat(CGFloat(lowerLimit), CGFloat(upperLimit+1)))
}

var hexColors = ["#00BCD4","#E91E63", "#9C27B0", "#4CAF50", "#009688", "#03A9F4", "#F44336", "#8BC34A","#2196F3", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548", "#9E9E9E", "#607D8B", "#000000"]

func getRandomColor()-> NSColor{
    let x = randomInt(hexColors.count)
    return NSColor(hexString: hexColors[x])
}

var lastColorIndex = -1
func getOnebyOneColor()-> NSColor{
    lastColorIndex += 1
    if lastColorIndex > hexColors.count - 1{
        lastColorIndex = 0
    }
    return NSColor(hexString: hexColors[lastColorIndex])
}

extension NSColor{
    
    public convenience init(hexString: String) {
        var cString:String = hexString.uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            print("not hex String")
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
        
    }
    
    
}





