//
//  RandomMove.swift
//  Rubik Square iOS
//
//  Created by Jamal on 10/27/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

// move
class RandomMove {
    
    var move: Move!
    var isRandomMoving = false
    
    var width: CGFloat{
        return move.selectedPlace!.frame.width
    }
    
    var height: CGFloat{
        return move.selectedPlace!.frame.height
    }
    
    
    var columnCount: Int{
        return move.places[0].count
    }
    
    var rowCount: Int{
        return move.places.count
    }
    
    init(move: Move) {
        self.move = move
    }
    
    func start(){
        if isRandomMoving{
            return
        }
        isRandomMoving = true
        repeatMove(step: 1)
    }
    
    func repeatMove(step: Int){
        if step == 40{
            isRandomMoving = false
            return
        }
        
        move.places[0][0].run(SKAction.sequence([
            moveAction(),
            SKAction.run {
                self.move.alignPosition()
                self.repeatMove(step: step + 1)
            }]))
    }
    
    func moveAction()-> SKAction{
        
        let rand = randomInt(1,2)
        let moveCode = rand == 1 ? moveRandomColumnCode(): moveRandomRowCode()
        
        return SKAction.repeat(SKAction.sequence([
            moveCode,
            SKAction.wait(forDuration: 0.03)
            ]), count: 7)
    }
    
    func selectRandomPlace()-> CGPoint{
        let row = randomInt(1, rowCount-2)
        let column = randomInt(1, columnCount-2)
        let place =  move.places[row][column]
        move.selectPlace(place: place, point: place.position)
        return CGPoint(x: place.position.x, y: place.position.y)
    }
    
    func moveRandomColumnCode()-> SKAction {
        
        var point = selectRandomPlace()
        let amount = height/7 + 1
        
        return SKAction.run {
            point.y += amount
            self.move.movePlaces(point: point)
        }
        
    }
    
    func moveRandomRowCode()-> SKAction {
        
        var point = selectRandomPlace()
        let amount = width/7 + 5
        
        return SKAction.run {
            point.x += amount
            self.move.movePlaces(point: point)
        }
        
    }
}


