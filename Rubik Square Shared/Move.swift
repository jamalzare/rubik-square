//
//  Move.swift
//  Rubik Square
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

enum MoveDirection{
    case left
    case right
    case top
    case bottom
}

enum Movement{
    case horizontal
    case vertical
}

class Move {
    
    var selectedPlace: Place?
    var places:[[Place]] = []
    var beforPosition: CGPoint?
    var movement: Movement? = nil
    var selectedPoint = CGPoint.zero
    var rowBasePositions: [CGPoint] = []
    var columnBasePositions: [CGPoint] = []
    var isRandomMoving = false
    
    var row: Int{
        return selectedPlace?.row ?? -1
    }
    
    var column: Int{
        return selectedPlace?.column ?? -1
    }
    
    var width: CGFloat{
        return selectedPlace!.frame.width
    }
    
    var height: CGFloat{
        return selectedPlace!.frame.height
    }
    
    var bx: CGFloat{
        return selectedPoint.x
    }
    
    var by: CGFloat{
        return selectedPoint.y
    }
    
    var position: CGPoint{
        return selectedPlace!.position
    }
    
    var columnCount: Int{
        return places[0].count
    }
    
    var rowCount: Int{
        return places.count
    }
    
    init(places: [[Place]]) {
        self.places = places
        
        for place in places[0]{
            rowBasePositions.append(place.position)
        }
        for row in 0...columnCount-1{
            columnBasePositions.append(places[row][0].position)
        }
    }
    
    func selectPlace(place: Place, point: CGPoint){
        selectedPlace = place
        beforPosition = place.position
        selectedPoint = point
    }
    
    func movePlaces(point: CGPoint){
        
        if movement == nil{
            
            if abs(point.y - by)>20 {
                movement = .vertical
            }
            else if abs(point.x - bx)>20 {
                movement = .horizontal
            }else{
                return
            }
        }
        
        movement == .horizontal ? moveHorizontal(point: point):
            moveVertical(point: point)
    }
    
    func alignPosition(){
        movement == .horizontal ?
            alignRowPosition(): alignColumnPosition()
        movement = nil
    }
    
    var c = 0
}


//--MARK: Horizontal Movment
extension Move{
    
    func alignRowPosition(){
        for (column, place) in places[row].enumerated(){
            place.position.x = rowBasePositions[column].x
            place.column = column
        }
        
        if row == 1 {
            for column in 0...columnCount-1{
                places[rowCount-1][column].color = places[1][column].color
            }
            return
        }
        if row == rowCount-2{
            for column in 0...columnCount-1{
                places[0][column].color = places[rowCount-2][column].color
            }
        }
    }
    
    func moveHorizontal(point: CGPoint){
        
        let direction: MoveDirection = point.x>position.x ? .right: .left
        
        if let place = selectedPlace {
            let amount = point.x - place.position.x
            moveRow(amount: amount)
            swapRowLastAndFirst(direction: direction)
        }
    }
    
    func moveRow(amount: CGFloat){
        for place in places[row]{
            place.position.x += amount
        }
    }
    
    func swapRowLastAndFirst(direction: MoveDirection){
        if width < abs(places[row][0].position.x - rowBasePositions[0].x){
            direction == .left ? swapLeft(): swapRight()
        }
    }
    
    func swapLeft(){
        
        let first = places[row].removeFirst()
        places[row].append(first)
        first.position.x = rowBasePositions[rowBasePositions.count-1].x
        first.color = places[row][1].color
    }
    
    func swapRight(){
        let last = places[row].popLast()
        places[row].insert(last!, at: 0)
        last?.position.x = rowBasePositions[0].x
        last?.color = places[row][columnCount-2].color
    }
    
}


//--MARK: Vertical Movment
extension Move{
    
    func alignColumnPosition(){
        for row in 0...rowCount - 1 {
            places[row][column].row = row
            places[row][column].position.y = columnBasePositions[row].y
        }
        
        if column == 1 {
            for row in 0...rowCount-1{
                places[row][columnCount-1].color = places[row][1].color
            }
            return
        }
        if column == columnCount-2{
            for row in 0...rowCount-1{
                places[row][0].color = places[row][columnCount-2].color
            }
        }
    }
    
    func moveVertical(point: CGPoint){
        
        let direction: MoveDirection = point.y>position.y ? .top: .bottom
        if let place = selectedPlace {
            let amount = point.y - place.position.y
            moveColumn(amount: amount)
            swapColumnLastAndFirst(direction: direction)
        }
    }
    
    func moveColumn(amount: CGFloat){
        for row in 0...rowCount - 1 {
            places[row][column].position.y += amount
        }
    }
    
    func swapColumnLastAndFirst(direction: MoveDirection){
        if height < abs(places[0][column].position.y - columnBasePositions[0].y){
            direction == .top ? swapTop(): swapBottom()
        }
    }
    
    func swapTop(){
        
        let last = places[rowCount-1][column]
        
        last.position.y = columnBasePositions[0].y
        
        for n in 0...rowCount-2{
            let row = rowCount-n-1
            places[row][column] = places[row - 1][column]
        }
        places[0][column] = last
        last.color = places[rowCount-2][column].color
    }
    
    func swapBottom(){
        
        let first = places[0][column]
        first.position.y = columnBasePositions[rowCount-1].y
        
        for row in 0...rowCount - 2{
            places[row][column] = places[row+1][column]
        }
        places[rowCount-1][column] = first
        first.color = places[1][column].color
    }
}

