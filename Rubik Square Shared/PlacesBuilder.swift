//
//  PlacesBuilder.swift
//  Rubik Square
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class PlacesBuilder{
    
    let node: SKNode!
    let rowsCount: Int!
    let colsCount: Int!
    var colors = [NSColor]()
    
    private var width: CGFloat = 0
    private var height: CGFloat = 0
    
    
    
    
    var places: [[Place]] = []
    
    init(area: CGRect, rowsCount: Int, colsCount: Int){
        
        let shape = SKShapeNode(rect: area)
        shape.fillColor = .clear
        shape.strokeColor = .clear
        self.node = shape
        
        self.rowsCount = rowsCount
        self.colsCount = colsCount
        
        width = node.frame.width/CGFloat(rowsCount)
        height = node.frame.height/CGFloat(colsCount)
        
        setColors()
        createPlaces()
    }
    
    func setColors(){
        for _ in 1...rowsCount{
            colors.append(getOnebyOneColor())
        }
        //colors = [.blue, .green, .yellow, .red]
    }
    
    func createPlaces(){
        
        for row in 0...rowsCount + 1{
            places.append([])
            for column in 0...colsCount + 1 {
                addPlace(row: row, column: column)
            }
        }
    }
    
    func addPlace(row: Int, column: Int){
        let place = createPlace(row: row, column: column)
        node.addChild(place)
        places[row].append(place)
        
    }
    
    func createPlace(row: Int, column: Int)-> Place{
       
        let size = CGSize(width: width, height: height)
        let place = Place(rectOf: size)
        
        place.position = calcPosition(row: row, column: column)
        
        place.index = (row * (rowsCount - 1)) + column
        place.row = row
        place.column = column
        place.createSquare()
        place.color = specifyColor(row: row, column: column)
        
        return place
    }
    
    func calcPosition(row: Int, column: Int)-> CGPoint{
        let x = node.frame.minX + CGFloat(column) * width - width/2
        let y = node.frame.minY + CGFloat(row) * height - height/2
        return CGPoint(x: x, y: y)
    }
    
    func specifyColor(row: Int, column: Int)-> NSColor{
       
        if row>0 && row < rowsCount + 1{
           return colors [row-1]
        }
        if row == 0{
            return colors[rowsCount-1]
        }
        if row == rowsCount+1{
            return colors[0]
        }
        return .black
    }
    
}


