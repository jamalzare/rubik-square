//
//  GameViewController.swift
//  Rubik Square macOS
//
//  Created by Jamal on 10/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit

class GameViewController: NSViewController {
    func presentScene(){
        if let view = self.view as? SKView {
            
            let scene = GameScene(size: CGSize(width: 1536, height: 2048))
            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        presentScene()
    }
    
}

